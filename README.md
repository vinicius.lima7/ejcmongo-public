# EJCMon GO

Projeto criado pela equipe de front-end da EJCM para a realização das aulas do *Treinamento Técnico 2022.2*. Neste projeto criamos uma aplicação móvel em React Native que simula uma interface do jogo Pokémon GO.

## 🗒 Tópicos abordados

- Fundamentos do React Native
- Estilização de páginas com Styled Components
- Componentes e Data Binding
- Formulários, Máscaras e Validação 
- Navegação entre páginas com uso de Rotas 
- Aplicação de Hooks
- Integração dos dados da interface com back-end 

## 🛠 Ferramentas utilizadas

- [TypeScript](https://www.typescriptlang.org/)
- [React Native](https://reactnative.dev/)
- [Expo](https://expo.dev/)
- [Styled Components](https://styled-components.com/)

## 🤝 Integrantes

- Bruno Pavese [@brunoejcm](https://gitlab.com/brunoejcm)
- Fernanda Bussi [@FLBussi](https://gitlab.com/FLBussi)
- Filipe Magalhães [fipmagdev](https://gitlab.com/fipmagdev)
- Thiago Barcellos [@ThBMattos](https://gitlab.com/ThBMattos)
- Vinícius Lima [@vinicius.lima7](https://gitlab.com/vinicius.lima7)

<hr>

## Como rodar o projeto?

Para rodar o projeto basta, entre primeiramente na pasta do projeto que contém o repositório:

```
cd ejcmon-go-copy
```

Uma vez na pasta do projeto, entre na pasta do front-end: 

```
cd front
```

Antes de partir para a execução, é necessário atualizar/instalar as dependências necessárias. Para tal, execute o comando:

```
yarn install
```

Tudo pronto! Basta rodar o projeto e ver como fica na web, no Android ou no IOS:

```
yarn start
```

<hr>

<div align="center">
    <p>README made with 🤍 by viny</p>    
</div>