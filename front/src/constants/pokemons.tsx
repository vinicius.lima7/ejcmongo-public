export const pokemons = [
    {
        index: 1,
        name: 'Charmander',
        type: 'fire',
        height: 8,
    }, 
    {
        index: 2,
        name: 'Bulbasaur',
        type: 'grass',
        height: 12,
    }, 
    {
        index: 3,
        name: 'Pikachu',
        type: 'electric',
        height: 5,
    },
    {
        index: 4,
        name: 'Squirtle',
        type: 'water',
        height: 6,
    },
]