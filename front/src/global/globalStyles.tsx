export const globalStyles = {
    colors: {
        primaryPurple: '#2B3151',
        secondaryPurple: '#353D63',
        lightGray: '#D9D9D9',
        offWhite: '#FAFAFA',
        yolkYellow: '#FFCD00',
        fireBg: '#FF9741',
        grassBg: '#38BF4B',
        waterBg: '#3692DC',
        electricBg: '#FBD100'
    },
    fonts: {
        r300: "Roboto_300Light",
        r400: "Roboto_400Regular",
        r500: "Roboto_500Medium",
        r700: "Roboto_700Bold",
        r900: "Roboto_900Black"
    }
}