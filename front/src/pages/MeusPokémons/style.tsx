import styled from "styled-components/native";
export const BtnsWrapper = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: space-evenly;
  margin-top: 28%;
`;
